﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StoreProductsAspNetMvs.Data;
using StoreProductsAspNetMvs.ViewModels;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using PagedList.Core;

namespace StoreProductsAspNetMvs.Controllers
{
  //  [Authorize(Roles = "admin")]
    public class ProductsController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IWebHostEnvironment _hosting;
        public ProductsController(IProductRepository productRepository, ICategoryRepository categoryRepository, IWebHostEnvironment hosting)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _hosting = hosting;
        }


        public IActionResult Index(int page = 1, int pageSize = 10)
        {
            PagedList<Product> products = new PagedList<Product>(_productRepository.GetAll(), page, pageSize);


            ViewBag.Title = "Products Index";
            return View(products);
        }

        public async Task<IActionResult> Create()
        {
            ProductCreateViewModel model = new ProductCreateViewModel();
            model.Categories = await _categoryRepository.GetAllAsync();
            return View(model);
        }



        [HttpPost]
        public async Task<IActionResult> Create(ProductCreateViewModel model)
        {
          
            if (ModelState.IsValid)
            {
                string uniqueFileName = null;
                if (model.Img != null)
                {
                    string uploadFolder = Path.Combine(_hosting.WebRootPath, "ProductsImg");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Img.FileName;
                    string FilePath = Path.Combine(uploadFolder, uniqueFileName);
                    model.Img.CopyTo(new FileStream(FilePath, FileMode.Create));
                }
                Product product = new Product()
                {
                    CategoryId = model.CategoryId,
                    Code = model.Code,
                    Count = model.Count,
                    IsFavorite = model.IsFavorite,
                    Name = model.Name,
                    Price = model.Price,
                    ImgPath = uniqueFileName,
                    Discount = model.Discount,
                    ShortDesc = model.ShortDesc,
                    LongDesc = model.LongDesc
                };
                _productRepository.Create(product);
                await _productRepository.SaveAsync();
                return RedirectToAction("Index", await _productRepository.GetAllAsync());

            }
            model.Categories = await _categoryRepository.GetAllAsync();
            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var product = _productRepository.GetById(id);
            EditProductViewModel editProductViewModel = new EditProductViewModel()
            {
                Id = product.Id,
                Name = product.Name,
                Code = product.Code,
                Count = product.Count,
                IsFavorite = product.IsFavorite,
                Price = product.Price,
                LongDesc = product.LongDesc,
                ShortDesc = product.ShortDesc,
                ExistImgPath = product.ImgPath

            };
            editProductViewModel.Categories = await _categoryRepository.GetAllAsync();
            return View(editProductViewModel);
        }

        // POST: Category/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(EditProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                Product product = _productRepository.GetById(model.Id);

                product.Name = model.Name;
                product.CategoryId = model.CategoryId;
                product.Code = model.Code;
                product.Count = model.Count;
                product.Discount = model.Discount;
                product.IsFavorite = model.IsFavorite;
                product.LongDesc = model.LongDesc;
                product.Price = model.Price;
                product.ShortDesc = model.ShortDesc;

                if (model.Img != null)
                {
                    if (model.ExistImgPath != null)
                    {
                        string filePath = Path.Combine(_hosting.WebRootPath, "ProductsImg", model.ExistImgPath);
                        System.IO.File.Delete(filePath);
                    }
                    product.ImgPath = ProcessUploadingFile(model);

                }

                _productRepository.Update(product);
                await _categoryRepository.SaveAsync();
                return RedirectToAction("Index");
            }
            return View();
        }

        private string ProcessUploadingFile(ProductCreateViewModel model)
        {
            string uniqueFileName = null;
            if (model.Img != null)
            {
                string uploadFolder = Path.Combine(_hosting.WebRootPath, "ProductsImg");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Img.FileName;
                string FilePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(FilePath, FileMode.Create))
                {
                    model.Img.CopyTo(fileStream);
                }

            }

            return uniqueFileName;
        }


    }
}