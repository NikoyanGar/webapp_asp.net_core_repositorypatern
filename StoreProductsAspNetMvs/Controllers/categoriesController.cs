﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedList.Core;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Models;
using StoreProductsAspNetMvs.ViewModels;

namespace StoreProductsAspNetMvs.Controllers
{
  //  [Authorize(Roles = "admin")]
    public class CategoriesController : Controller
    {
        public CategoriesController(ICategoryRepository categoryRepository, IWebHostEnvironment hosting)
        {
            _categoryRepository = categoryRepository;
            _hosting = hosting;
        }
        private readonly ICategoryRepository _categoryRepository;

        private readonly IWebHostEnvironment _hosting;

        public IActionResult Index(int page = 1, int pageSize = 10)
        {
            PagedList<Category> categories = new PagedList<Category>(_categoryRepository.GetAll(), page, pageSize);


            ViewBag.Title = "Products Index";
            return View(categories);
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        public async Task<ActionResult> Create(CategoryCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = ProcessUploadingFile(model);
              
                Category category = new Category()
                {
                    Name = model.Name,
                    ImgPath = uniqueFileName,
                };
                _categoryRepository.Create(category);
                await _categoryRepository.SaveAsync();
                return View();
            }
            return RedirectToAction("Index", await _categoryRepository.GetAllAsync());
        }
        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
          var category=  _categoryRepository.GetById(id);
            EditCategoryViewModel editCategoryViewModel = new EditCategoryViewModel()
            {
                Id = category.Id,
                Name = category.Name,
                ExistImgPath = category.ImgPath

            };
            return View(editCategoryViewModel);
        }

        // POST: Category/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(EditCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                Category category = _categoryRepository.GetById(model.Id);
                category.Name = model.Name;
                if (model.Img!=null)
                {
                    if (model.ExistImgPath!=null)
                    {
                      string filePath=  Path.Combine(_hosting.WebRootPath, "CategoryImg", model.ExistImgPath);
                        System.IO.File.Delete(filePath);///????????
                    }
                   category.ImgPath= ProcessUploadingFile(model);

                }
             
                _categoryRepository.Update(category);
                await _categoryRepository.SaveAsync();
                return RedirectToAction("Index");
            }
            return View();
        }

       
        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Category/Delete/5
        [HttpPost]
        public async Task< ActionResult> Delete(int id, IFormCollection collection)
        {
            try
            {
                _categoryRepository.Delete(id);
                await _categoryRepository.SaveAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        private string ProcessUploadingFile(CategoryCreateViewModel model)
        {
            string uniqueFileName = null;
            if (model.Img != null)
            {
                string uploadFolder = Path.Combine(_hosting.WebRootPath, "CategoryImg");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Img.FileName;
                string FilePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream= new FileStream(FilePath, FileMode.Create))
                {
                    model.Img.CopyTo(fileStream);
                }
              
            }

            return uniqueFileName;
        }

    }
}