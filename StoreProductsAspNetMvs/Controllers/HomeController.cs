﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PagedList.Core;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Models;

namespace StoreProductsAspNetMvs.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;

        public HomeController(IProductRepository productRepository, ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;

        }
        public async Task<IActionResult> Index()
        {
            ViewBag.Title = "Mhers Store";
            return View(await _categoryRepository.GetAllAsync());
        }

        public IActionResult Categories(int page = 1, int pageSize = 8)
        {
            PagedList<Category> categories = new PagedList<Category>(_categoryRepository.GetAll(), page, pageSize);


            ViewBag.Title = "Products categories";
            return View(categories);
        }




        public IActionResult Products( int Id, int page = 1, int pageSize = 8)
        {
           
            PagedList<Product> products = new PagedList<Product>(_productRepository.GetByCategory(Id), page, pageSize);

            ViewBag.Title = "Products Index";
            return View(products);
        }

       
     
        public async Task<IActionResult> ProductDetails(int Id)
        {
            var product = await _productRepository.GetByIdAsynq(Id);

            ViewBag.Title = $"Product{product.Name}";
            return View(product);
        }

        public IActionResult Search(int page = 1, int pageSize = 8)
        {
            var keyWord = Request.Query["keyword"].ToString();
            ViewBag.Title = "Serach";
            ViewBag.keyword = keyWord;
            PagedList<Product> products = new PagedList<Product>(_productRepository.GetByName(keyWord), page, pageSize);

            return View(products);
        }


    }
}