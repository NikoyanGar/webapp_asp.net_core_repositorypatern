﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StoreProductsAspNetMvs.Data;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Mocks;
using StoreProductsAspNetMvs.Data.Models;
using StoreProductsAspNetMvs.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Service
{
    public static class ServiceExtention
    {
        public static IServiceCollection AddContextService(this IServiceCollection services, IConfiguration configuration)
        {
            string connection = configuration.GetConnectionString("DefaultConnection");
            //string connection = "Data Source=desktop-e9f87gh\\sqlexpress;Initial Catalog=Store;Integrated Security=True;";
            services.AddDbContext<StoreProdContext>(options =>
               options.UseSqlServer(connection));
            services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<StoreProdContext>();
            return services;
        }
        public static IServiceCollection AddRepositoryService(this IServiceCollection services)
        {
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            return services;

        }
    }
}
