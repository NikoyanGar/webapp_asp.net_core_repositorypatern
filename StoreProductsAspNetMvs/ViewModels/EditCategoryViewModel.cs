﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.ViewModels
{
    public class EditCategoryViewModel:CategoryCreateViewModel
    {
        public int Id { get; set; }
        public string  ExistImgPath { get; set; }
    }
}
