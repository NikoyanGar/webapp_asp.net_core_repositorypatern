﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.ViewModels
{
    public class CreateUserViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password,ErrorMessage ="invalid passvord")]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string UserName { get; set; }
    }
}
