﻿using Microsoft.AspNetCore.Http;
using StoreProductsAspNetMvs.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.ViewModels
{
    public class ProductCreateViewModel
    {
        [Required]
        [MaxLength(15, ErrorMessage = "Max lenght 15 ch")]
        public string Name { get; set; }
        [Required]
        [MaxLength(20,ErrorMessage ="Max lenght 20 ch")]
        public string Code { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }
        [Required]
        public int Count { get; set; }
       
        public bool IsFavorite { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Max lenght 50 ch")]
        public string ShortDesc { get; set; }
        [Required]
        public double Discount { get; set; }
        public double DiscountPrice => Price - Price * Discount / 100;
        public string LongDesc { get; set; }
        public List<Category> Categories { get; set; }

        [Required]
        public IFormFile Img { get; set; }
    }
}
