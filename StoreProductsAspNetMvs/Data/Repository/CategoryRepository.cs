﻿using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Repository
{
    public class CategoryRepository : BaseRepository<Category, int>, ICategoryRepository
    {
        private readonly StoreProdContext _context;
        public CategoryRepository(StoreProdContext context):base(context)
        {
            _context = context;
        }

        public Category GetByKey(string name)
        {
           return _context.Categories.FirstOrDefault(c => c.Name.Contains(name));
        }
    }
}
