﻿using Microsoft.EntityFrameworkCore;
using StoreProductsAspNetMvs.Data.Interfacees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Repository
{
    public abstract class BaseRepository<TEntity, TKey> : IBaseRepository<TEntity, TKey> where TEntity : class, new()
    {
        private readonly StoreProdContext _context;
        public BaseRepository(StoreProdContext context)
        {
            _context = context;
        }
       
        public void Create(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Delete(TKey key)
        {
          var entity= _context.Set<TEntity>().Find(key);
            _context.Set<TEntity>().Remove(entity);
        }
        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }
        public IEnumerable<TEntity> GetAll()
        {
            return  _context.Set<TEntity>();
        }

        public TEntity GetById(TKey key)
        {
            return _context.Set<TEntity>().Find(key);
        }

        public async Task<TEntity> GetByIdAsynq(TKey key)
        {
            return await _context.Set<TEntity>().FindAsync(key);
        }

        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
        }

        public Task SaveAsync()
        {
           return _context.SaveChangesAsync();
        }
    }
}
