﻿using Microsoft.EntityFrameworkCore;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Repository
{
    public class ProductRepository : BaseRepository<Product, int>, IProductRepository
    {
        private readonly StoreProdContext _context;
        public ProductRepository(StoreProdContext context):base(context)
        {
            _context = context;
        }
        public new async Task<List<Product>> GetAllAsync()
        {
           return await _context.Products.Include(c=>c.Category).ToListAsync();
        }

        public async Task<List<Product>> GetByCategoryAsync(int categoryId)
        {
            var category = await _context.Categories.Where(c=>c.Id==categoryId).Include(c => c.Products).FirstAsync();
            return category.Products;
        }
        public IEnumerable<Product> GetByCategory(int categoryId)
        {

            return _context.Products.Where(p => p.CategoryId == categoryId);
        }

        public async Task<List<Product>> GetFavAllAsync()
        {
            return await _context.Products.Where(p=>p.IsFavorite).Include(c => c.Category).ToListAsync();
        }

        public IEnumerable<Product> GetByName(string name)
        {
           return  _context.Products.Where(p => p.Name.Contains(name));
        }
        public IEnumerable<Product> GetByCategoryName(string name)
        {
            var products = _context.Products.Include(p=>p.Category).Where(c => c.Category.Name.Contains(name));
            return products;
        }

        public IEnumerable<Product> GetByCode(string code)
        {
            return _context.Products.Where(p => p.Code.Contains(code));
        }
    }
}
