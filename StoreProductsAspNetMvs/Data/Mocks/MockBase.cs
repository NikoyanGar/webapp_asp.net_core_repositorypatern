﻿using StoreProductsAspNetMvs.Data.Interfacees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Mocks
{
    public abstract class MockBase<TEntity, TKey> : IBaseRepository<TEntity, TKey> where TEntity : class, new()
    {
        public void Create(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(TKey key)
        {
            throw new NotImplementedException();
        }

        public List<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<List<TEntity>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public TEntity GetById(TKey key)
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> GetByIdAsynq(TKey key)
        {
            throw new NotImplementedException();
        }

        public Task SaveAsync()
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        IEnumerable<TEntity> IBaseRepository<TEntity, TKey>.GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
