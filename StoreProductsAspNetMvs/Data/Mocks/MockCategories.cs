﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Models;

namespace StoreProductsAspNetMvs.Data.Mocks
{
    public class MockCategories : MockBase<Category,int>,ICategoryRepository
    {
        
       public new List<Category> GetAll()
        {
            return new List<Category>()
            {
                new Category{ Name="Gorciq" , Id=1},
                new Category { Name="Lusatu", Id=2},
                new Category { Name="Jaher", Id=3}

            };
        }

        public Category GetByKey(string name)
        {
            throw new NotImplementedException();
        }
    }
}
