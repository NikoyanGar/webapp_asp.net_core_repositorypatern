﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreProductsAspNetMvs.Data.Interfacees;
using StoreProductsAspNetMvs.Data.Mocks;
using StoreProductsAspNetMvs.Data.Models;

namespace StoreProductsAspNetMvs.Data.Mocks
{
    public class MockProducts : MockBase<Product,int>,IProductRepository
    {
       
       public new List<Product> GetAll()
        {
            return new List<Product>
            {
                new Product{ Name="led20-20" , Count=25, Price=45.4, Code="Abc123", ImgPath="img/1.jpg" },
                new Product{ Name="chaguch" , Count=12, Price=100.5, Code="Abb163", ImgPath="img/1.jpg"},
                new Product{ Name="atvyortka" , Count=24, Price=85.4, Code="Acc123",ImgPath="img/1.jpg"},
                new Product{ Name="led45W" , Count=25, Price=45.4, Code="Abbb123" ,ImgPath="img/2.jpg"},
                new Product{ Name="60-60" , Count=10, Price=1005, Code="Acccc123",ImgPath="img/4.jpg"},
                new Product{ Name="transformator" , Count=32, Price=45.4, Code="A466123",ImgPath="img/2.jpg"}
            };
        }

        public Task<List<Product>> GetByCategory(int categoryId)
        {
            throw new NotImplementedException();
        }

        public Task<List<Product>> GetByCategoryAsync(int categoryId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetByCategoryName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetByCode(string code)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetByName(string name)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Product> IProductRepository.GetByCategory(int categoryId)
        {
            throw new NotImplementedException();
        }
    }
}
