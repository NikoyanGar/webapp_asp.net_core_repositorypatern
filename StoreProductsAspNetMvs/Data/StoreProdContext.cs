﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StoreProductsAspNetMvs.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data
{
    public class StoreProdContext: IdentityDbContext<User>
    {
        public StoreProdContext(DbContextOptions<StoreProdContext> options):base(options)
        {

        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
      

    }
}
