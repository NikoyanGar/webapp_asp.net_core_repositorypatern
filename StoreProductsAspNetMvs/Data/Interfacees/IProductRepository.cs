﻿using StoreProductsAspNetMvs.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Interfacees
{
   public interface IProductRepository:IBaseRepository<Product,int>
    {
        public Task<List<Product>> GetByCategoryAsync(int categoryId);
        public IEnumerable<Product> GetByCategory(int categoryId);
        public IEnumerable<Product> GetByName(string name);
        public IEnumerable<Product> GetByCode(string code);
        public IEnumerable<Product> GetByCategoryName(string name);


    }
}
