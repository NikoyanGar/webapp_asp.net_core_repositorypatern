﻿using StoreProductsAspNetMvs.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Interfacees
{
    public interface ICategoryRepository : IBaseRepository<Category, int>
    {
        public Category GetByKey(string name);

    }
}
