﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Interfacees
{
    public interface IBaseRepository<Tentity, TKey> where Tentity : class, new()
    {
        public IEnumerable<Tentity> GetAll();
        public Task<List<Tentity>> GetAllAsync();
        public Tentity GetById(TKey key);
        public Task<Tentity> GetByIdAsynq(TKey key);
        public void Create(Tentity entity);
        public void Delete(TKey key);
        public void Update(Tentity entity);
        public Task SaveAsync();
    }
}
