﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreProductsAspNetMvs.Data.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ImgPath { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
        public bool IsFavorite { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public double Discount { get; set; }

        public Category Category { get; set; }
        public int CategoryId { get; set; }


    }
}
